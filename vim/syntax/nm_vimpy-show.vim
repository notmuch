" notmuch show mode syntax file

setlocal conceallevel=2
setlocal concealcursor=vinc

syntax region  nmMessage     matchgroup=Ignore concealends start='[0-9]\+\/-*message start-*\\' end='\\-*message end-*\/' fold contains=@nmShowMsgBody keepend

"TODO what about those
syntax cluster nmShowMsgDesc contains=nmShowMsgDescWho,nmShowMsgDescDate,nmShowMsgDescTags
syntax match   nmShowMsgDescWho /[^)]\+)/ contained
syntax match   nmShowMsgDescDate / ([^)]\+[0-9]) / contained
syntax match   nmShowMsgDescTags /([^)]\+)$/ contained

syntax cluster nmShowMsgBody contains=@nmShowMsgBodyMail,@nmShowMsgBodyGit
syntax include @nmShowMsgBodyMail syntax/mail.vim
silent! syntax include @nmShowMsgBodyGit syntax/notmuch-git-diff.vim

highlight nmShowMsgDescWho term=reverse cterm=reverse gui=reverse
highlight link nmShowMsgDescDate Type
highlight link nmShowMsgDescTags String

"TODO what about this?
highlight Folded term=reverse ctermfg=LightGrey ctermbg=Black guifg=LightGray guibg=Black
