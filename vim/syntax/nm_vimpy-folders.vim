" notmuch folders mode syntax file

syntax region nmFolfers             start=/^/ end=/$/         oneline contains=nmFoldersMessageCount
syntax match  nmFoldersMessageCount /^ *[0-9]\+ */            contained nextgroup=nmFoldersUnreadCount
syntax match  nmFoldersUnreadCount  /(.\{-}) */               contained nextgroup=nmFoldersName
syntax match  nmFoldersName         /.*\ze(/                  contained nextgroup=nmFoldersSearch
syntax match  nmFoldersSearch       /([^()]\+)$/

highlight link nmFoldersMessageCount Statement
highlight link nmFoldersUnreadCount  Underlined
highlight link nmFoldersName         Type
highlight link nmFoldersSearch       String

highlight CursorLine term=reverse cterm=reverse gui=reverse

