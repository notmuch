syntax region nmSearch          start=/^/ end=/$/       oneline contains=nmSearchDate keepend
syntax match nmSearchDate       /^.\{-13}/              contained nextgroup=nmSearchNum skipwhite
syntax match nmSearchNum        "[0-9]\+\/"             contained nextgroup=nmSearchTotal skipwhite
syntax match nmSearchTotal      /[0-9]\+/               contained nextgroup=nmSearchFrom skipwhite
syntax match nmSearchFrom       /.\{-}\ze|/             contained nextgroup=nmSearchSubject skipwhite
"XXX this fails on some messages with multiple authors
syntax match nmSearchSubject    /.*\ze(/                contained nextgroup=nmSearchTags,nmUnread
syntax match nmSearchTags       /.\+$/                  contained
syntax match nmUnread           /.*\<unread\>.*)$/      contained

highlight link nmSearchDate    Statement
highlight link nmSearchNum     Number
highlight link nmSearchTotal   Type
highlight link nmSearchFrom    Include
highlight link nmSearchSubject Normal
highlight link nmSearchTags    String

highlight link nmUnread        Underlined
